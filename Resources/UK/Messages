#
# Messages for !MakeModes
#

TaskID:MakeModes
_Version:99 red balloons

#
# Errors list
#

SAVEAS:Drag this icon to the directory in which you want to save the file. Or, drag it to the program into which you want to transfer the data.

MNF:I cannot find this monitor in my list, please select the appropriate monitor from the monitors menu
CONFAI:I cannot open the PreDesk.Configure file, unable to restore configured mode file.
MONFAI:I cannot open the MonList file for writing
0:Unknown error number %d
1:Error %d (%s) when calling SWI %X
2:Internal error: cannot load template %s
3:Internal error: attempt to set text of unsuitable icon (to %s)
4:File name must be fully qualified
5:Two modes have the name \"%s\"
6:The file \"%s\" could not be opened
7:MakeModes can only handle one file at a time
8:Error in mode file \"%s\" at line %d: \"%s\" expected
9:File \"%s\" contains no mode data
10:Unexpected end of file \"%s\" after %d line%s
11:File format %d is not supported
12:No memory to build menu
13:Required fields of current mode are not frozen
14:Timing totals of current mode do not match required fields
15:Cannot delete the only mode
16:No memory to store new mode
17:You must choose a unique name for a new mode
18:The current mode's name is not unique
19:No memory to store comments
20:Drag comments to the text icon; drag modefiles to the icon bar
21:A non-empty monitor title is required
22:The %d%s mode has no name
23:Drag a text file to the comments icon to replace the modefile comments
24:No standard modes match the given parameters
25:Internal error: cannot open monitor types file
26:No memory to store monitor details
27:Nonsensical line and/or frame rate values
28:No monitor types are defined in the MonModes file
29:Failed to open \"%s\"
30:Internal error: cannot open monitors file
31:Cannot open file for writing
32:The last square pixel mode in the list must have a mode name
33:Individual modes must be saved with a mode name
34:Cannot find mode in mode chain
35:This machine has more VRAM than I know about!

#
# Menus
#

IBarMenuT:MakeModes
IBarMenuH:>Info,VESA,Quit
IBHelp0:Move the pointer right to see information about !MakeModes.
IBHelp1:Click here to select VESA timing standards.
IBHelp2:Click here to Quit MakeModes.

DispMenuT:Main
DispMenuH:>Save MDF,>Generate list,>Monitor info,New mode,Delete mode,Comments
DispHelp0:Move the pointer right to save the ModeFile.
DispHelp1:Move the pointer right to save a list of the modes present in this ModeFile.
DispHelp2:Move the pointer right to see information on the currently selected monitor.
DispHelp3:Click here to create a new mode. This will copy the paramaters for the existing mode.
DispHelp4:Click here to delete the currently selected mode.
DispHelp5:Click here to edit the ModeFile comments. The edited comments file should then be dragged back to the Comments icon for inclusion in the mode file.

BppMenuT:Pixel Depth
BppMenuH:1 bpp (Black/white), 2 bpp (4 Greys), 4 bpp (16 Colours), 8 bpp (256 Colours), 16 bpp (32 thousand), 32 bpp (16 million)
BppHelp0:Click here to select 1bpp mode, 2 colours, black and white.
BppHelp1:Click here to select 2bpp mode, 4 greys.
BppHelp2:Click here to select 4bpp mode, 16 greys or colours.
BppHelp3:Click here to select 8bpp mode, 256 greys or colours.
BppHelp4:Click here to select 16bpp mode, 32 thousand colours.
BppHelp5:Click here to select 32bpp mode, 16 million colours.

#
# Main help text
#

MMHelp:This is the !MakeModes icon.
MGTitle:This is the current monitor title.
MGTitNM:Click here for a list of available monitors.
MGType:This is the monitor type icon.
MGdpms:Use this icon to cycle through monitor types.
MGminh:This is the minimum line rate for the selected monitor.
MGmaxh:This is the maximum line rate for the selected monitor.
MGminv:This is the minimum frame rate for the selected monitor.
MGmaxv:This is the maximum frame rate for the selected monitor.
MGtol:This is the monitors maximum tolerance level.
MGtolA:Use this arrow to alter the monitors tolerance level.
MGgen:Click here to generate a set of modes for the selected monitor.

MDhelp1:This is the currently selected monitor title.
MDhelp2:Click here for a list of available monitors.
MDhelp3:This is the file format icon.
MDhelp4:This is the number of modes currently available.
MDhelp5:This is the currently selected DPMS state.
MDhelp6:Click here to select a new DPMS state.
MDhelp7:Drag a text file here to replace the original modefile comments.
MDhelp8:Click here for a list of available modes.
MDhelp9:Click here to enter a new mode name.
MDhelp10:When this icon is ticked, the mode name will be included in the mode file.
MDhelp11:This is the x-resolution icon.
MDhelp12:This is the y-resolution icon.
MDhelp13:Click here to test the currently selected mode.
MDhelp14:Click here to enter a new pixel rate.
MDhelp15:This is the sync polarity icon.
MDhelp16:Click here to change the sync pol value.
MDhelp17:This is the current Frame rate icon.
MDhelp18:This is the current Line rate icon.
MDhelp19:Calculations will be made for a 0MB VRAM machine.
MDhelp20:Calculations will be made for a 1MB VRAM machine.
MDhelp21:Calculations will be made for a 2MB VRAM machine.
MDhelp22:This is the currently selected Bits Per Pixel icon.
MDhelp23:Click here to change the currently selected Bits Per Pixel.
MDhelp24:Screen memory required for this screen mode.
MDhelp25:Bandwidth required for this screen mode.
MDhelp26:Information about screen mode timings will appear here.
MDhelp27:This is the horizontal sync width icon.
MDhelp28:This is the horizontal back porch icon.
MDhelp29:This is the left hand border icon.
MDhelp30:This is the horizontal display size icon.
MDhelp31:This is the right hand border icon.
MDhelp32:This is the front porch icon.
MDhelp33:This is the vertical sync width icon.
MDhelp34:This is the vertical back porch icon.
MDhelp35:This is the top border icon.
MDhelp36:This is the vertical display size icon.
MDhelp37:This is the bottom border icon.
MDhelp38:This is the vertical front porch icon.
MDhelp39:This is the horizontal timings totals icon.
MDhelp40:This is the vertical timings totals icon.
MDhelp41:This icon holds a copy of the horizontal timings totals for reference. Click here to copy the new horizontal timings totals. This value must match the horizontal timings totals before a ModeFile can be saved.
MDhelp42:This icon holds a copy of the vertical timings totals for reference. Click here to copy the new vertical timings totals. This value must match the vertical timings totals before a ModeFile can be saved.
MDhelp43:When this icon is ticked, the porch and border totals will be locked.

InfoHelp:This window gives information on the MakeModes application.

MChelp1:Click here to discard the current mode file.
MChelp2:Click here to cancel this action.
MChelp3:Click here to save the current mode file.
